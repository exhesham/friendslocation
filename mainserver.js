//JKLMdb43dskb  exheshamdb  mongodb://exheshamdb:JKLMdb43dskb@jello.modulusmongo.net:27017/ut3umenI
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var TOKEN = process.env.TOKEN;
var databaseAPI = require( __dirname + '/Utils/FLDatabaseAPI');

app.all('/updatelocation', function(req, res) {
		console.log("updatelocation:",req.body);
		if(req.body.TOKEN != TOKEN){
			res.status(500).send("not authorized");
			return;
		}
		databaseAPI.updateData(req.body).then(function(data){
			console.log("Finished ok");
			
			res.status(200).send("OK!");
		}).catch(function(err){
			console.log("Finished err",err);
			
			res.status(500).send(JSON.stringify(err));
		});
});
app.all('/readlocation', function(req, res) {
		console.log("readlocation:",req.body);
		if(req.body.TOKEN != TOKEN){
			res.status(500).send("not authorized");
			return;
		}
		databaseAPI.findData(req.body).then(function(data){
			console.log("Finished ok");
			
			res.status(200).send(JSON.stringify(data));
			
		}).catch(function(err){
			console.log("Finished err",err);
			//res.writeHead(500, {'Content-Type': 'text/plain'});
			res.status(500).send(JSON.stringify(err));
			
		});
});

var port = process.env.PORT || 8080;
app.listen(port,"0.0.0.0");

console.log('Express server started on port %s', port);
