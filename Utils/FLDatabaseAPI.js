var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url =process.env.MONGO_DB_URL || "mongodb://admin:abc123@172.30.166.235/coordinationsdb";


console.log("The database mongo url is:"+url);
// mongo --port 27017 -u root -p abcKLM14 --authenticationDatabase admin

var dbname = "coordinations";
var db=null;

MongoClient.connect(url, function(err, dbOut) {
  console.log("returned new db. err=",err);
  db= dbOut;
  
});
function connectToDB(){
	console.log("connectToDB called")
	return new Promise(function(resolve,reject){
		if(db != null){
			console.log("We have connectivity - db != null...finishing")
			resolve(db);
		}else{			
			console.log("connectToDB: db == null, will create new one")
			MongoClient.connect(url, function(err, dbOut) {
			  console.log("returned new db. err=",err);
			  db= dbOut;
			  if(err){
				reject(err);
			  }else{
				  
				resolve(dbOut);
			  }
			  
			});
		}
   });	
}

var insertData = function(datajson) {
	
   return new Promise(function(resolve,reject){
	   
	   datajson._id=datajson.userID;
		db.collection(dbname).insertOne(datajson, function(err, result) {
			console.log("Inserted data into the coordinations collection.err=",err);
			if(err){
				reject(err);
			}else{
				resolve(result);
			}
		});
   });
};

var updateData = function(data) {
	return new Promise(function(resolve,reject){
		console.log("call connectToDB...")
		connectToDB().then(function(newdb){
			console.log("Updating data.userID="+data.userID)	
			data._id=data.userID;
		   newdb.collection(dbname).update(
			  { "_id" : data.userID },
			  data,
				{ upsert: true }
		   , function(err, results) {
				console.log(err);
				if(err){
					reject(err);
				}else{
					resolve(null);
				}
		   });
			
		}).catch(reject);
	});
};

var findData = function(reqJson) {
	console.log("findData: reqJson",reqJson)
	var promises = [];
	var users = [];
	var	userIDs = reqJson.userIDs;
	return new Promise(function(resolveMain,rejectMain){
		connectToDB().then(function(newdb){
					for(i in userIDs){
			console.log("Handling userIDs["+i+"]=",userIDs[i]);
			promises.push(new Promise(function(resolve,reject){
					newdb.collection(dbname).findOne( { "userID": userIDs[i]},function(find_err,user_record){
						if(find_err){
							console.log("error  for index"+i+"=",find_err);
							reject(null);
						}else{
							if(user_record != null  && user_record != undefined){
								users.push(user_record);
							}
							console.log("found a record for index"+i+"=",user_record);
							resolve(user_record);
						}
					});
				}));
		}
		Promise.all(promises).then(function(res){
			console.log("all found users were promised:",users);
			resolveMain(users);
		});
			
		}).catch(rejectMain);
		

	});
};
module.exports.insertData = insertData;
module.exports.updateData = updateData;
module.exports.findData = findData;